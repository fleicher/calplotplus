"""
Calendar heatmaps from Pandas time series data.

Plot Pandas time series data sampled by day in a heatmap per calendar year.
"""

import calendar
import datetime
from dateutil.relativedelta import relativedelta

import numpy as np
import pandas as pd

from matplotlib.colors import ColorConverter, ListedColormap, to_rgba
from matplotlib.patches import Polygon
from matplotlib.patheffects import withStroke
import matplotlib.pyplot as plt


def yearplot(data, year=None, how='sum',
             vmin=None, vmax=None,
             cmap='viridis', fillcolor='whitesmoke',
             linewidth=1, linecolor=None, edgecolor='gray',
             daylabels=None, dayticks=True,
             dropzero=None,
             textformat=None, textfiller='', textcolor_dark='black', textcolor_light='white',
             textoverlay=None, textsize=None, stroke_kws=None,
             monthlabels=None, monthlabeloffset=15,
             monthticks=True, horizontalorientation=True,
             ax=None, **kwargs):
    """
    Plot one year from a timeseries as a calendar heatmap.

    Parameters
    ----------
    data : Series
        Data for the plot. Must be indexed by a DatetimeIndex.
    year : integer
        Only data indexed by this year will be plotted. If `None`, the first
        year for which there is data will be plotted.
    how : string
        Method for resampling data by day. If `None`, assume data is already
        sampled by day and don't resample. Otherwise, this is passed to Pandas
        `Series.resample`.
    vmin, vmax : floats
        Values to anchor the colormap. If `None`, min and max are used after
        resampling data by day.
    cmap : matplotlib colormap name or object
        The mapping from data values to color space.
    fillcolor : matplotlib color
        Color to use for days without data.
    linewidth : float
        Width of the lines that will divide each day.
    linecolor : color
        Color of the lines that will divide each day. If `None`, the axes
        background color is used, or 'white' if it is transparent.
    daylabels : list or bool
        Strings to use as labels for days, must be of length 7.
    dayticks : list or int or bool
        If `True`, label all days. If `False`, don't label days. If a list,
        only label days with these indices. If an integer, label every n day.
    dropzero : bool
        If `True`, don't fill a color for days with a zero value.
    monthlabels : list
        Strings to use as labels for months, must be of length 12.
    monthlabeloffset : integer
        Day offset for labels for months to adjust horizontal alignment.
    monthticks : list or int or bool
        If `True`, label all months. If `False`, don't label months. If a
        list, only label months with these indices. If an integer, label every
        n month.
    edgecolor : color
        Color of the lines that will divide months.
    textformat : string
        Text format string for grid cell text
    textfiller : string
        Fallback text for grid cell text for cells with no data
    textcolor_dark : color
        Color of the grid cell text when the background is light for better contrast
    textcolor_light : color
        Color of the grid cell text when the background is dark for better contrast
    textoverlay : Series
        These values are displayed in the cells (specified in series index)
    textsize: float or str
        Fontsize for text in cells. Possible text values:
        {'xx-small', 'x-small', 'small', 'medium', 'large', 'x-large', 'xx-large'}
    horizontalorientation : boolean
        Determine if years are displayed as rows (True) or columns (False)
    ax : matplotlib Axes
        Axes in which to draw the plot, otherwise use the currently-active
        Axes.
    kwargs : other keyword arguments
        All other keyword arguments are passed to matplotlib `ax.pcolormesh`.

    Returns
    -------
    ax : matplotlib Axes
        Axes object with the calendar heatmap.

    """

    if year is None:
        year = data.index.sort_values()[0].year

    if how is None:
        # Assume already sampled by day.
        by_day = data
    else:
        # Sample by day.
        by_day = data.resample('D').agg(how)

    # Default to dropping zero values for a series with over 50% of rows being zero.
    if dropzero is not False and by_day[by_day == 0].count() > 0.5 * by_day.count():
        dropzero = True

    if dropzero:
        by_day = by_day.replace({0: np.nan}).dropna()

    if isinstance(cmap, str):
        cmap = plt.get_cmap(cmap)

    # Min and max per day.
    if vmin is None:
        vmin = by_day.min()
    if vmax is None:
        vmax = by_day.max()
    if monthlabels is None:
        monthlabels = calendar.month_abbr[1:]
    if daylabels is None:
        daylabels = calendar.day_abbr[:] if horizontalorientation else [c[0] for c in calendar.day_abbr[:]]

    if ax is None:
        ax = plt.gca()

    if linecolor is None:
        # Unfortunately, linecolor cannot be transparent, as it is drawn on
        # top of the heatmap cells. Therefore it is only possible to mimic
        # transparent lines by setting them to the axes background color. This
        # of course won't work when the axes itself has a transparent
        # background so in that case we default to white which will usually be
        # the figure or canvas background color.
        linecolor = ax.get_facecolor()
        if ColorConverter().to_rgba(linecolor)[-1] == 0:
            linecolor = 'white'

    def shape_to_year(series_by_day):
        # Filter on year.
        series_by_day = series_by_day[str(year)] if series_by_day is not None \
            else pd.Series({datetime.datetime(year, 1, 1): np.nan})

        # Add missing days.
        series_by_day = series_by_day.reindex(
            pd.date_range(start=str(year), end=str(year + 1),
                          freq='D', tz=series_by_day.index.tzinfo)[:-1])

        # Create data frame we can pivot later.
        series_by_day = pd.DataFrame({'data': series_by_day,
                                      'fill': 1,
                                      'day': series_by_day.index.dayofweek,
                                      'week': series_by_day.index.isocalendar().week})

        # There may be some days assigned to previous year's last week or
        # next year's first week. We create new week numbers for them so
        # the ordering stays intact and week/day pairs unique.
        series_by_day.loc[(series_by_day.index.month == 1) & (series_by_day.week > 50), 'week'] = 0
        series_by_day.loc[(series_by_day.index.month == 12)
                          & (series_by_day.week < 10), 'week'] = series_by_day.week.max() + 1
        return series_by_day

    by_day = shape_to_year(by_day)

    # Pivot data on day and week and mask NaN days.
    plot_data = by_day.pivot('day', 'week', 'data').values[::-1]
    plot_data = np.ma.masked_where(np.isnan(plot_data), plot_data)

    # Do the same for all days of the year, not just those we have data for.
    fill_data = by_day.pivot('day', 'week', 'fill').values[::-1]
    fill_data = np.ma.masked_where(np.isnan(fill_data), fill_data)

    # Do the same for the text layer on top
    text_data = shape_to_year(textoverlay).pivot('day', 'week', 'data').values[::-1]

    # Draw heatmap for all days of the year with fill color.
    ax.pcolormesh(fill_data if horizontalorientation else fill_data[::-1, ::-1].T,
                  vmin=0, vmax=1, cmap=ListedColormap([fillcolor]))

    # Draw heatmap.
    kwargs['linewidth'] = linewidth
    kwargs['edgecolors'] = linecolor
    ax.pcolormesh(plot_data if horizontalorientation else plot_data[::-1, ::-1].T,
                  vmin=vmin, vmax=vmax, cmap=cmap, **kwargs)

    # Limit heatmap to our data.
    ax.set(xlim=(0, plot_data.shape[1 if horizontalorientation else 0]),
           ylim=(0, plot_data.shape[0 if horizontalorientation else 1]))

    # Square cells.
    ax.set_aspect('equal')

    # Remove spines and ticks.
    for side in ('top', 'right', 'left', 'bottom'):
        ax.spines[side].set_visible(False)
    for axis in (ax.xaxis, ax.yaxis):
        axis.set_tick_params(which='both', length=0)

    # Get indices for monthlabels.
    if monthticks is True:
        monthticks = range(len(monthlabels))
    elif monthticks is False:
        monthticks = []

    # Get indices for daylabels.
    if dayticks is True:
        dayticks = range(len(daylabels))
    elif dayticks is False:
        dayticks = []

    ax.set_xlabel('')
    ax.set_ylabel('')

    monthticks_dt = [by_day.loc[pd.Timestamp(datetime.date(year, i + 1, monthlabeloffset))].week for i in monthticks]
    dayticks_dt = [6 - i + 0.5 for i in dayticks]

    if horizontalorientation:
        ax.set_xticks(monthticks_dt)
        ax.set_xticklabels([monthlabels[i] for i in monthticks])

        ax.yaxis.set_ticks_position('right')
        ax.set_yticks(dayticks_dt)
        ax.set_yticklabels([daylabels[i] for i in dayticks], rotation='horizontal', va='center')
    else:
        ax.set_yticks(monthticks_dt[::-1])
        ax.set_yticklabels([monthlabels[i] for i in monthticks])

        ax.xaxis.set_ticks_position('top')
        ax.set_xticks(dayticks_dt[::-1])
        ax.set_xticklabels([daylabels[i] for i in dayticks], ha='center')

    # Text in mesh grid if format is specified or specified text is present.
    if textformat is not None or (~pd.isna(text_data)).sum() > 0:
        for y in range(plot_data.shape[0]):
            for x in range(plot_data.shape[1]):
                content = ''
                masked = plot_data[y, x]
                if not pd.isna(text_data[y, x]):
                    content = str(text_data[y, x])
                else:
                    if textformat is None:
                        continue
                    if masked is np.ma.masked:
                        if fill_data[y, x] == 1:
                            content = textfiller
                    else:
                        content = textformat.format(masked)
                background_color = (to_rgba(fillcolor) if masked is np.ma.masked
                                    else cmap((plot_data[y, x] - vmin) / (vmax-vmin)))
                print((plot_data[y, x] - vmin) / (vmax-vmin))
                (ax.text(x + .5 if horizontalorientation else plot_data.shape[0] - 0.5 - y,
                         y + .5 if horizontalorientation else plot_data.shape[1] - 0.5 - x,
                         content, color=textcolor_dark if background_color[0]*76.544 + background_color[1]*150.272
                                                          + background_color[2]*29.184 > 150 else textcolor_light,
                         ha='center', va='center', fontsize=textsize)
                   .set_path_effects([withStroke(**({} if stroke_kws is None else stroke_kws))]))

    # Month borders code credited to https://github.com/rougier/calendar-heatmap
    start = datetime.datetime(year, 1, 1).weekday()
    number_weeks = (
        (datetime.datetime(year, 12, 31) - datetime.timedelta(days=datetime.datetime(year, 1, 1).weekday()))
        - (datetime.datetime(year, 1, 1) - datetime.timedelta(days=datetime.datetime(year, 12, 31).weekday()))
    ).days // 7

    for month in range(1, 13):
        first = datetime.datetime(year, month, 1)
        last = first + relativedelta(months=1, days=-1)
        y0 = 7 - first.weekday()
        y1 = 7 - last.weekday()
        x0 = (int(first.strftime('%j'))+start-1)//7
        x1 = (int(last.strftime('%j'))+start-1)//7
        P = [(x0, y0),
             (x0+1, y0),
             (x0+1, 7),
             (x1+1, 7),
             (x1+1, y1-1),
             (x1, y1-1),
             (x1, 0),
             (x0, 0) ]
        if not horizontalorientation:
            P = np.flip(P, axis=1)
            P[:, 1] = number_weeks - P[:, 1] + 1
            P[:, 0] = 7 - P[:, 0]
        poly = Polygon(P, edgecolor=edgecolor, facecolor='None',
                       linewidth=linewidth, zorder=20, clip_on=False)
        ax.add_artist(poly)

    return ax


def calplot(data, how='sum',
            yearlabels=True, yearascending=True,
            yearlabel_kws=dict(), subplot_kws=dict(), gridspec_kws=dict(),
            figsize=None, horizontalorientation=True, fig_kws=dict(), colorbar=None,
            suptitle=None, suptitle_kws=dict(),
            tight_layout=True, **kwargs):
    """
    Plot a timeseries as a calendar heatmap.

    Parameters
    ----------
    data : Series
        Data for the plot. Must be indexed by a DatetimeIndex.
    how : string
        Method for resampling data by day. If `None`, assume data is already
        sampled by day and don't resample. Otherwise, this is passed to Pandas
        `Series.resample`.
    figsize : (float, float)
        Size of figure for the plot.
    suptitle : string
        Title for the plot.
    yearlabels : bool
       Whether or not to draw the year label for each subplot.
    yearascending : bool
       Sort the calendar in ascending or descending order.
    horizontalorientation : boolean
        Determine if years are displayed as rows (True) or columns (False)
    yearlabel_kws : dict
       Keyword arguments passed to the matplotlib `set_ylabel` call which is
       used to draw the year for each subplot.
    subplot_kws : dict
        Keyword arguments passed to the matplotlib `subplots` call.
    gridspec_kws : dict
        Keyword arguments passed to the matplotlib `GridSpec` constructor used
        to create the grid the subplots are placed on.
    fig_kws : dict
        Keyword arguments passed to the matplotlib `subplots` call.
    suptitle_kws : dict
        Keyword arguments passed to the matplotlib `suptitle` call.
    kwargs : other keyword arguments
        All other keyword arguments are passed to `yearplot`.

    Returns
    -------
    fig, axes : matplotlib Figure and Axes
        Tuple where `fig` is the matplotlib Figure object `axes` is an array
        of matplotlib Axes objects with the calendar heatmaps, one per year.

    """

    years = np.unique(data.index.year)
    if not yearascending:
        years = years[::-1]

    if colorbar is None:
        colorbar = data.nunique() > 1

    if figsize is None:
        figsize = (10+(colorbar*2.5), 1.7*len(years))
    
    fig, axes = plt.subplots(nrows=len(years), ncols=1, squeeze=False,
                             figsize=figsize,
                             subplot_kw=subplot_kws,
                             gridspec_kw=gridspec_kws, **fig_kws)
    axes = axes.T[0]

    # We explicitly resample by day only once. This is an optimization.
    by_day = data
    if how is not None:
        by_day = by_day.resample('D').agg(how)

    ylabel_kws = dict(
        fontsize=30,
        color='gray',
        fontname='Helvetica',
        fontweight='bold',
        ha='center')
    ylabel_kws.update(yearlabel_kws)

    max_weeks = 0

    for year, ax in zip(years, axes):
        yearplot(by_day, year=year, how=None, ax=ax, horizontalorientation=horizontalorientation,
                 **kwargs)
        max_weeks = max(max_weeks, ax.get_xlim()[1])

        if yearlabels:
            ax.set_ylabel(str(year), **ylabel_kws)

    # In a leap year it might happen that we have 54 weeks (e.g., 2012).
    # Here we make sure the width is consistent over all years.
    for ax in axes:
        ax.set_xlim(0, max_weeks)

    stitle_kws = dict()

    if tight_layout:
        plt.tight_layout()
        stitle_kws.update({'y': 1})

    if colorbar:
        if tight_layout:
            stitle_kws.update({'x': 0.425, 'y': 1.03})

        orientation = 'vertical' if horizontalorientation else 'horizontal'
        if len(years) == 1:
            fig.colorbar(axes[0].get_children()[1], ax=axes.ravel().tolist(),
                         orientation=orientation)
        else:
            if horizontalorientation:
                fig.subplots_adjust(right=0.8)
            else:
                fig.subplots_adjust(bottom=0.2)
            fig.colorbar(axes[0].get_children()[1], cax=cax, orientation=orientation)

    stitle_kws.update(suptitle_kws)
    plt.suptitle(suptitle, **stitle_kws)

    return fig, axes
